#!/bin/sh

USER="${1}"
IMAGE="${2}"
TAG="${3}"

docker build -t "${USER}"/"${IMAGE}":"${TAG}" -t "${USER}"/"${IMAGE}":latest .
docker push "${USER}"/"${IMAGE}"
