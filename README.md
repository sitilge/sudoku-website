# sudoku-website

A website for the sudoku solver. It comes with a minimal, elegant, and responsive (bootstrap based) UI, where the user can solve the sudoku, clear the grid, or download all solutions as a JSON file.

It also displays the error, e.g. an existing number, and a hostname, which is useful in scaled setups, e.g. seeing on which node the code runs.

![Sudoku solver website](assets/sudoku.png)

# Running

## Docker
The easiest way is via docker:

````
docker run -p 80:80 sitilge/sudoku:latest
````

You can also run build the image yourself from the Dockerfile. The image size is kept to minimum, thanks to Docker multi-stage builds.

## Go
Alternatively, you can run it locally:

````
go get -v && go build main.go && sudo ./main
````

Access the website at `http://localhost:80`.

## CI/CD

A GitLab CI/CD has been configured with 6 stages for automatic formatting, testing, building, handling the docker image, and deploying on staging and production environments on AWS EKS. You have to have the underlying AWS EKS infrastructure (see the related project: https://git.sitilge.id.lv/sitilge/sudoku-devops) in place. Then set up the following values in GitLab CI/CD variables section:

- `CI_REGISTRY_USER` - Docker Hub username.
- `CI_REGISTRY_PASSWORD` - Docker Hub access token or password (not recommended).
- `AWS_ACCESS_KEY_ID` - AWS IAM user access key ID.
- `AWS_SECRET_ACCESS_KEY` - AWS IAM user access key secret.
- `AWS_DEFAULT_REGION` - AWS default region.
- `AWS_CLUSTER_NAME` - the name of the EKS cluster.

Finally, rerun the GitLab CI/CD pipeline and access the DNS of the load balancer.