#Multi-stage build
ARG dir=/go/src/git.sitilge.id.lv/sitilge/sudoku-website

#Build stage
FROM golang:latest as build
ARG dir
WORKDIR $dir
COPY . .
RUN go get -v
#Have to use the netgo flag for static linking
RUN go build --tags netgo main.go

#Run stage
#FROM alpine:latest as run
#3.12 was not supported on Sep 1, 2020 for ECR image scan
FROM alpine:3.11 as run
ARG dir
WORKDIR /root
#Create a dir for logs
RUN mkdir ./logs
#Copy the static files
COPY --from=build $dir/web ./web
#Copy the binary
COPY --from=build $dir/main .
CMD ["./main"]