module git.sitilge.id.lv/sitilge/sudoku-website

go 1.15

require (
	git.sitilge.id.lv/sitilge/sudoku-solver v0.0.4
	github.com/felixge/httpsnoop v1.0.1
)
