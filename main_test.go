package main

import (
	"reflect"
	"testing"
)

func TestHandleInput(t *testing.T) {
	type args struct {
		values map[string][]string
	}
	tests := []struct {
		name    string
		args    args
		want    [9][9]uint8
		wantErr bool
	}{
		{
			name: "Valid values",
			args: args{
				values: map[string][]string{
					"row-0": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-1": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-2": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-3": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-4": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-5": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-6": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-7": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-8": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
				},
			},
			want: [9][9]uint8{
				0: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				1: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				2: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				3: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				4: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				5: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				6: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				7: {0, 1, 2, 3, 4, 5, 6, 7, 8},
				8: {0, 1, 2, 3, 4, 5, 6, 7, 8},
			},
			wantErr: false,
		},
		{
			name: "Invalid key",
			args: args{
				values: map[string][]string{
					"row-0":   {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-abc": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-2":   {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
				},
			},
			want:    [9][9]uint8{},
			wantErr: true,
		},
		{
			name: "Invalid key",
			args: args{
				values: map[string][]string{
					"row-0": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"abc":   {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-2": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
				},
			},
			want:    [9][9]uint8{},
			wantErr: true,
		},
		{
			name: "Invalid digit, larger than 4 bits",
			args: args{
				values: map[string][]string{
					"row-0": {"999", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-1": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
					"row-2": {"0", "1", "2", "3", "4", "5", "6", "7", "8"},
				},
			},
			want:    [9][9]uint8{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := HandleInput(tt.args.values)
			if (err != nil) != tt.wantErr {
				t.Errorf("HandleInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("HandleInput() = %v, want %v", got, tt.want)
			}
		})
	}
}
