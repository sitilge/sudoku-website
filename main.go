package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	solver "git.sitilge.id.lv/sitilge/sudoku-solver"
	"github.com/felixge/httpsnoop"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

//TODO - maybe logrus?
const LogPath string = "logs"
const LogFilenameAccess string = "access.log"
const LogFilenameError string = "error.log"
const LogLevelFatal int = 1
const LogLevelInfo int = 2

//ErrorHandler handles errors.
type ErrorHandler struct {
	templates *template.Template
}

//HomeHandler handles requests made to /.
type HomeHandler struct {
	templates *template.Template
}

//SolveHandler handles requests made to /solve.
type SolveHandler struct {
	templates *template.Template
}

//HandleError handles general errors.
func (e ErrorHandler) HandleError(response http.ResponseWriter, code int, template string, text string) {
	response.WriteHeader(code)

	if template == "" {
		_, err := response.Write([]byte(text))
		if err != nil {
			LogEvent(LogFilenameError, LogLevelInfo, err.Error())
		}

		return
	}

	err := e.templates.ExecuteTemplate(response, template, struct {
		Code int
		Text string
	}{
		Code: code,
		Text: text,
	})
	if err != nil {
		LogEvent(LogFilenameError, LogLevelFatal, err.Error())
	}
}

//ServeHTTP serves GET requests made to /.
func (h HomeHandler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodGet {
		ErrorHandler(h).HandleError(response, http.StatusMethodNotAllowed, "error", http.StatusText(http.StatusMethodNotAllowed))

		return
	}

	if request.URL.Path != "/" {
		ErrorHandler(h).HandleError(response, http.StatusNotFound, "error", http.StatusText(http.StatusNotFound))

		return
	}

	cmd := exec.Command("cat", "/etc/hostname")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		LogEvent(LogFilenameError, LogLevelFatal, err.Error())
	}

	var data = struct {
		Hostname string
	}{
		Hostname: out.String(),
	}

	err = h.templates.ExecuteTemplate(response, "home", data)
	if err != nil {
		LogEvent(LogFilenameError, LogLevelFatal, err.Error())
	}
}

//ServeHTTP serves POST requests made to /solve.
func (h SolveHandler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {
		ErrorHandler(h).HandleError(response, http.StatusMethodNotAllowed, "", http.StatusText(http.StatusMethodNotAllowed))

		return
	}

	if request.URL.Path != "/solve" {
		ErrorHandler(h).HandleError(response, http.StatusNotFound, "", http.StatusText(http.StatusNotFound))

		return
	}

	err := request.ParseForm()
	if err != nil {
		ErrorHandler(h).HandleError(response, http.StatusBadRequest, "", err.Error())

		return
	}

	cells, err := HandleInput(request.PostForm)
	if err != nil {
		ErrorHandler(h).HandleError(response, http.StatusBadRequest, "", err.Error())

		return
	}

	matrix, err := solver.NewMatrix(cells)
	if err != nil {
		ErrorHandler(h).HandleError(response, http.StatusBadRequest, "", err.Error())

		return
	}

	solutions, err := matrix.Solve()
	if err != nil {
		ErrorHandler(h).HandleError(response, http.StatusInternalServerError, "", err.Error())

		return
	}

	output, err := json.Marshal(solutions)
	if err != nil {
		ErrorHandler(h).HandleError(response, http.StatusInternalServerError, "", err.Error())

		return
	}

	_, err = response.Write(output)
	if err != nil {
		LogEvent(LogFilenameError, LogLevelInfo, err.Error())
	}
}

//LogEvent logs events in files based on the event level.
func LogEvent(filename string, level int, text string) {
	file, err := os.OpenFile(fmt.Sprintf("%v/%v", LogPath, filename), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	if level == LogLevelFatal {
		log.Fatalln(text)
	}

	_, err = file.Write([]byte(text))
	if err != nil {
		log.Fatalf("error closing file: %v", err)
	}

	err = file.Close()
	if err != nil {
		log.Fatalf("error closing file: %v", err)
	}
}

//HandleInput takes in the values from the matrix form and outputs
//the parsed matrix cells. It does no validation on digits / logic.
func HandleInput(values map[string][]string) ([9][9]uint8, error) {
	//The values must be in the form:
	//map[row-0:[ 4 2      5] row-1:[   6 3 2  8 ] row-2:[ 8   4  2  ] ...
	cells := [9][9]uint8{}

	for name, row := range values {
		//The values should be "row-#".
		split := strings.Split(name, "-")
		if len(split) != 2 {
			return [9][9]uint8{}, errors.New("invalid key")
		}

		i, err := strconv.Atoi(split[1])
		if err != nil {
			return [9][9]uint8{}, err
		}

		for j, value := range row {
			if value == "" {
				cells[i][j] = 0

				continue
			}

			parsed, err := strconv.ParseUint(value, 10, 4)
			if err != nil {
				return [9][9]uint8{}, err
			}

			cells[i][j] = uint8(parsed)
		}
	}

	return cells, nil
}

//Logger is a middleware to log all requests.
func Logger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		//It would be non-trivial to get metrics such as bytes written and response code
		//But there is a project for that...
		metrics := httpsnoop.CaptureMetrics(handler, writer, request)

		entry := fmt.Sprintf("%v [%v] %v %v %v %v %v %v [%v] %v\n",
			request.RemoteAddr,
			time.Now().Format(time.RFC822Z),
			request.Method,
			request.URL,
			request.Proto,
			metrics.Code,
			metrics.Written,
			metrics.Duration,
			request.Header.Get("User-Agent"),
			request.Header.Get("Referer"),
		)

		if metrics.Code >= 400 && metrics.Code < 500 {
			LogEvent(LogFilenameError, LogLevelInfo, entry)

			return
		}

		LogEvent(LogFilenameAccess, LogLevelInfo, entry)

		//No need to call ServeHTTP as httpsnoop.CaptureMetrics does that already
		//handler.ServeHTTP(writer, request)
	})
}

func main() {
	addr := flag.String("addr", "0.0.0.0:80", "The address to use for listening")
	flag.Parse()

	mux := http.NewServeMux()

	//Set the static file server.
	staticHandler := http.FileServer(http.Dir("web"))
	mux.Handle("/static/", Logger(staticHandler))

	//Parse the templates.
	templates, err := template.ParseGlob("web/templates/*")
	if err != nil {
		LogEvent(LogFilenameError, LogLevelFatal, err.Error())
	}

	//Set the solve handler.
	solveHandler := SolveHandler{
		templates: nil,
	}
	mux.Handle("/solve", Logger(solveHandler))

	//Set the home handler.
	homeHandler := HomeHandler{
		templates: templates,
	}
	mux.Handle("/", Logger(homeHandler))

	server := http.Server{
		Addr:              *addr,
		Handler:           mux,
		ReadTimeout:       5 * time.Second,
		ReadHeaderTimeout: 5 * time.Second,
		WriteTimeout:      5 * time.Second,
		IdleTimeout:       60 * time.Second,
	}

	err = server.ListenAndServe()
	if err != nil {
		LogEvent(LogFilenameError, LogLevelFatal, err.Error())
	}
}
