function validate(ele) {
    ele.value = ele.value.replace(/[^1-9]/g,'');
}

$(function () {
    $("#form").on("keydown", function(event) {
        let focus = $(":focus")[0]
        let split = focus.id.split("-")

        let row = parseInt(split[1])
        let column = parseInt(split[2])

        if (event.key === "ArrowUp") {
            row -= 1
        } else if (event.key === "ArrowDown") {
            row += 1
        } else if (event.key === "ArrowLeft") {
            column -= 1
        } else if (event.key === "ArrowRight") {
            column += 1
        }

        $("#cell-" + row + "-" + column).trigger("focus")
    })

    $("#solve").on("click", function () {
        $.ajax({
            type: "POST",
            url: "/solve",
            contentType: "application/x-www-form-urlencoded",
            data: $("#form").serializeArray(),
            dataType: "json",
            success: function (response) {
                $("#error").html("")

                if (Object.values(response).length === 0) {
                    return
                }

                let download = $("#download")
                let data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response));
                download.removeClass("disabled")
                download.attr("href", data)
                download.attr("download", "sudoku.json")
                
                $("#sudoku .input").each(function (index, ele) {
                    let input = $(ele)
                    let split = ele.id.split("-")
                    let row = parseInt(split[1])
                    let column = parseInt(split[2])

                    if (input.val() !== "") {
                        //TODO - use classes
                        input.css("background-color", "grey")
                    } else {
                        const {Rows} = response;
                        input.val(Rows[row][column])
                    }
                })
            },
            error: function (response) {
                let download = $("#download")
                download.addClass("disabled")

                $("#error").html(response.responseText)
            }
        });

        return false
    });

    $("#clear").on("click", function () {
        $("#error").html("")

        let download = $("#download")
        download.addClass("disabled")

        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                let cell = $(".cell-" + i + "-" + j)
                cell.val("")
                //TODO - use classes
                cell.css("background-color", "#FAFAFA")
            }
        }
    });

    for (let i = 0; i < 9; i++) {
        let tr = $("#sudoku").append("<tr class=\"row-" + i + "\">")
        for (let j = 0; j < 9; j++) {
            tr.append("<td><input id=\"cell-" + i + "-" + j + "\" class=\"input cell-" + i + "-" + j + "\" name=\"row-" + i + "\" maxlength=\"1\" oninput=\"validate(this);\" /></td>")
            let input = $("#cell-" + i + "-" + j)

            if (i % 3 === 0) {
                input.addClass("border-top-2")
            }

            if (i === 8) {
                input.addClass("border-bottom-2")
            }

            if (j % 3 === 0) {
                input.addClass("border-left-2")
            }

            if (j === 8) {
                input.addClass("border-right-2")
            }
        }
    }

    //Display some dummy values.
    if (Math.random() * 10 < 5) {
        defaultValues0()
    } else {
        defaultValues1()
    }

    function defaultValues0() {
        // $(".cell-0-0").val(0)
        $(".cell-0-1").val(4)
        $(".cell-0-2").val(2)
        // $(".cell-0-3").val(0)
        // $(".cell-0-4").val(0)
        // $(".cell-0-5").val(0)
        // $(".cell-0-6").val(0)
        // $(".cell-0-7").val(0)
        $(".cell-0-8").val(5)

        // $(".cell-1-0").val(0)
        // $(".cell-1-1").val(0)
        // $(".cell-1-2").val(0)
        $(".cell-1-3").val(6)
        $(".cell-1-4").val(3)
        $(".cell-1-5").val(2)
        // $(".cell-1-6").val(0)
        $(".cell-1-7").val(8)
        // $(".cell-1-8").val(0)

        // $(".cell-2-0").val(0)
        $(".cell-2-1").val(8)
        // $(".cell-2-2").val(0)
        // $(".cell-2-3").val(0)
        $(".cell-2-4").val(4)
        // $(".cell-2-5").val(8)
        $(".cell-2-6").val(2)
        // $(".cell-2-7").val(0)
        // $(".cell-2-8").val(0)

        // $(".cell-3-0").val(0)
        // $(".cell-3-1").val(0)
        // $(".cell-3-2").val(0)
        // $(".cell-3-3").val(0)
        // $(".cell-3-4").val(0)
        // $(".cell-3-5").val(0)
        // $(".cell-3-6").val(0)
        // $(".cell-3-7").val(0)
        // $(".cell-3-8").val(0)

        $(".cell-4-0").val(7)
        $(".cell-4-1").val(1)
        $(".cell-4-2").val(5)
        // $(".cell-4-3").val(0)
        $(".cell-4-4").val(6)
        $(".cell-4-5").val(8)
        $(".cell-4-6").val(3)
        $(".cell-4-7").val(4)
        // $(".cell-4-8").val(0)

        $(".cell-5-0").val(9)
        // $(".cell-5-1").val(0)
        $(".cell-5-2").val(8)
        $(".cell-5-3").val(3)
        $(".cell-5-4").val(5)
        // $(".cell-5-5").val(0)
        $(".cell-5-6").val(7)
        $(".cell-5-7").val(6)
        $(".cell-5-8").val(1)

        // $(".cell-6-0").val(0)
        $(".cell-6-1").val(9)
        $(".cell-6-2").val(1)
        // $(".cell-6-3").val(0)
        // $(".cell-6-4").val(0)
        $(".cell-6-5").val(6)
        // $(".cell-6-6").val(0)
        // $(".cell-6-7").val(0)
        // $(".cell-6-8").val(0)

        // $(".cell-7-0").val(0)
        // $(".cell-7-1").val(0)
        // $(".cell-7-2").val(0)
        // $(".cell-7-3").val(0)
        $(".cell-7-4").val(2)
        // $(".cell-7-5").val(0)
        $(".cell-7-6").val(1)
        $(".cell-7-7").val(9)
        // $(".cell-7-8").val(0)

        // $(".cell-8-0").val(0)
        // $(".cell-8-1").val(0)
        $(".cell-8-2").val(6)
        $(".cell-8-3").val(1)
        // $(".cell-8-4").val(0)
        // $(".cell-8-5").val(0)
        // $(".cell-8-6").val(0)
        $(".cell-8-7").val(5)
        // $(".cell-8-8").val(0)
    }

    function defaultValues1() {
        $(".cell-0-0").val(5)
        // $(".cell-0-1").val(0)
        // $(".cell-0-2").val(0)
        // $(".cell-0-3").val(0)
        $(".cell-0-4").val(1)
        // $(".cell-0-5").val(0)
        // $(".cell-0-6").val(0)
        // $(".cell-0-7").val(0)
        // $(".cell-0-8").val(0)

        // $(".cell-1-0").val(0)
        $(".cell-1-1").val(9)
        // $(".cell-1-2").val(0)
        $(".cell-1-3").val(4)
        // $(".cell-1-4").val(0)
        // $(".cell-1-5").val(0)
        $(".cell-1-6").val(6)
        $(".cell-1-7").val(3)
        // $(".cell-1-8").val(0)

        $(".cell-2-0").val(3)
        // $(".cell-2-1").val(0)
        // $(".cell-2-2").val(0)
        // $(".cell-2-3").val(0)
        // $(".cell-2-4").val(0)
        // $(".cell-2-5").val(8)
        $(".cell-2-6").val(9)
        // $(".cell-2-7").val(0)
        // $(".cell-2-8").val(0)

        // $(".cell-3-0").val(0)
        // $(".cell-3-1").val(0)
        // $(".cell-3-2").val(0)
        // $(".cell-3-3").val(0)
        // $(".cell-3-4").val(0)
        // $(".cell-3-5").val(0)
        // $(".cell-3-6").val(0)
        // $(".cell-3-7").val(0)
        $(".cell-3-8").val(7)

        // $(".cell-4-0").val(0)
        // $(".cell-4-1").val(0)
        $(".cell-4-2").val(1)
        $(".cell-4-3").val(6)
        // $(".cell-4-4").val(0)
        // $(".cell-4-5").val(0)
        // $(".cell-4-6").val(0)
        // $(".cell-4-7").val(0)
        // $(".cell-4-8").val(0)

        // $(".cell-5-0").val(0)
        // $(".cell-5-1").val(0)
        // $(".cell-5-2").val(0)
        $(".cell-5-3").val(1)
        $(".cell-5-4").val(9)
        // $(".cell-5-5").val(0)
        $(".cell-5-6").val(2)
        // $(".cell-5-7").val(0)
        $(".cell-5-8").val(5)

        $(".cell-6-0").val(7)
        // $(".cell-6-1").val(0)
        // $(".cell-6-2").val(0)
        // $(".cell-6-3").val(0)
        // $(".cell-6-4").val(0)
        // $(".cell-6-5").val(0)
        // $(".cell-6-6").val(0)
        // $(".cell-6-7").val(0)
        // $(".cell-6-8").val(0)

        // $(".cell-7-0").val(0)
        $(".cell-7-1").val(8)
        // $(".cell-7-2").val(0)
        $(".cell-7-3").val(9)
        // $(".cell-7-4").val(0)
        $(".cell-7-5").val(4)
        $(".cell-7-6").val(1)
        // $(".cell-7-7").val(0)
        // $(".cell-7-8").val(0)

        $(".cell-8-0").val(2)
        // $(".cell-8-1").val(0)
        // $(".cell-8-2").val(0)
        // $(".cell-8-3").val(0)
        $(".cell-8-4").val(3)
        // $(".cell-8-5").val(0)
        // $(".cell-8-6").val(0)
        $(".cell-8-7").val(4)
        // $(".cell-8-8").val(0)
    }
});